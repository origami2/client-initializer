var assert = require('assert');
var Client = require('origami-client');
var ClientInitializer = require('..');
var EventEmitter = require('events').EventEmitter;

describe('ClientInitializer', function () {
  it('requires a resolution function', function () {
    assert.throws(
      function () {
        new ClientInitializer();
      },
      /resolution function is required/
    );
  });
  
  it('returns a function', function () {
    assert.equal('function', typeof(new ClientInitializer(function () {})));
  });
  
  it('function resolves', function (done) {
    var fakeSocket = new EventEmitter();
    
    (new ClientInitializer(function () {}))(fakeSocket)
    .then(function () {
      done();
    });
  });
  
  it('function resolves a ready function', function (done) {
    var fakeSocket = new EventEmitter();
    
    (new ClientInitializer(function () {}))(fakeSocket)
    .then(function (ready) {
      try {
        assert.equal('function', typeof(ready));
        
        done();
      } catch (e) {
        done(e);
      }
    });
  });
  
  it('when the function is ready, it emits describe-apis', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      try {
        assert.equal('function', typeof(callback));
        
        done();
      } catch (e) {
        done(e);
      }
    });
    
    (new ClientInitializer(function () {}))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('when describe-apis callback is called successfully, resolves a client builder function', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            'echo': [ 'what' ]
          }
        }
      );
    });
    
    (new ClientInitializer(
      function (clientBuilder) {
        try {
          assert.equal('function', typeof(clientBuilder));
        
          done();
        } catch (e) {
          done(e);
        }
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('when client builder is invoked, a Client is created', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            'echo': [ 'what' ]
          }
        }
      );
    });
    
    (new ClientInitializer(
      function (clientBuilder) {
        try {
          var client = clientBuilder();
        
          assert(client);
          assert(client.StubPlugin1);
          assert(client.StubPlugin1.echo);
          
          done();
        } catch (e) {
          done(e);
        }
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('when client method is invoked api message is sent', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            'echo': [ 'what' ]
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'api', 
      function (stackToken, pluginName, methodName, params, callback) {
        try {
          assert(!stackToken);
          assert.equal('StubPlugin1', pluginName);
          assert.equal('echo', methodName);
          assert.deepEqual(
            {
              what: 'hello',
            },
            params
          );
          assert.equal('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    (new ClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder();
        
        client.StubPlugin1.echo('hello');
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('stackToken is preserved', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            'echo': [ 'what' ]
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'api', 
      function (stackToken, pluginName, methodName, params, callback) {
        try {
          assert.deepEqual({some: 'token' }, stackToken);
          assert.equal('StubPlugin1', pluginName);
          assert.equal('echo', methodName);
          assert.deepEqual(
            {
              what: 'hello',
            },
            params
          );
          assert.equal('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    (new ClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder({some:'token'});
        
        client.StubPlugin1.echo('hello');
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('resolves promise when api callback is invoked with result', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            'echo': [ 'what' ]
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'api', 
      function (stackToken, pluginName, methodName, params, callback) {
        callback(null, 'hello');
      }
    );
    
    (new ClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder();
        
        client.StubPlugin1.echo('hello')
        .then(function (result) {
          try {
            assert.equal('hello', result);
            
            done();
          } catch (e) {
            done(e);
          }
        });
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
  
  it('resolves promise when api callback is invoked with error', function (done) {
    var fakeSocket = new EventEmitter();
    
    fakeSocket
    .on('describe-apis', function (callback) {
      callback(
        null,
        {
          'StubPlugin1': {
            'echo': [ 'what' ]
          }
        }
      );
    });
    
    fakeSocket
    .on(
      'api', 
      function (stackToken, pluginName, methodName, params, callback) {
        callback('my error');
      }
    );
    
    (new ClientInitializer(
      function (clientBuilder) {
        var client = clientBuilder();
        
        client.StubPlugin1.echo('hello')
        .catch(function (error) {
          try {
            assert.equal('my error', error);
            
            done();
          } catch (e) {
            done(e);
          }
        });
      }
    ))(fakeSocket)
    .then(function (ready) {
      ready();
    });
  });
});
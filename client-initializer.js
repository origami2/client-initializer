var Client = require('origami-client');

function PenelopeClientFactory(onResolution, onRejection, notificate) {
  return function (socket, params) {
    socket
    .emit(
      'describe-apis',
      function (err, apis) {
        if (err) return onRejection(err);
        
        onResolution(function (token) {
          return new Client(socket, apis, token, notificate);
        });      
      }
    );

    return Promise.resolve();
  };
}

module.exports = PenelopeClientFactory;